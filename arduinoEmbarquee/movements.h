// Libraries included
#include "Grove_I2C_Motor_Driver.h"
#include "mapStorage.h"
#include "mapSetup.h"

// Constants
#ifndef MOVEMENTS_H
#define MOVEMENTS_H

#define I2CADDR 0xf   // Motors' default address

// Different speed sets for the motors'set
#define FULLSPEED 100
#define REDUCEDSPEED 90
#define TURNSPEED 68
#define BACKWARDTURNSPEED -60
#define CORRECTIONSPEED 25
#define BIGCORRECTIONSPEED 0
#define STARTINGSPEED 60

// Digital ports, identification of the sensors ; L : Left, EL : ExtremeLeft, R : Right, ER : Extreme Right
#define SIGNALPINEL 5
#define SIGNALPINL 6
#define SIGNALPINR 7
#define SIGNALPINER 8

// Digital ports for the diagnosis LEDs
#define PIN_LEFT_LED 10
#define PIN_CENTER_LED 11
#define PIN_RIGHT_LED 12

// The different delays used
#define JUNCTION 0
#define START 1500
#define DELAYBEFORETURN 80
#define DELAYBEFORETURNSLOW 120
#define TURNMINDURATION 400
#define STRAIGHTJUNCTDURATION 100
#define CORRAFTERTURNDURATION 15
#define TIMEBETWEENJUNCS 200
#define ACCELERATIONTIME 500

// The enum declaring the different states available
enum StatesEnum {
  FORWARD,
  LEFT_JUNCTION,
  RIGHT_JUNCTION,
  FORWARD_JUNCTION,
  AFTER_TURN_CORRECTION
};

typedef enum StatesEnum States;

// Moving functions

void turnLeft();
void turnRight();

void correctRight(int);
void correctLeft(int);

void bigCorrectRight(int);
void bigCorrectLeft(int);

void goStraight(int);

void initJunction(Point**, Orientation*, States*);

void endLeftJunction(States*, long*);
void endRightJunction(States*, long*);
void endStraightJunction(States*, long*);

void junction(Point**, Orientation*);

bool isRightTurn(Orientation, Orientation);
bool isLeftTurn(Orientation, Orientation);
bool isNoTurn(Orientation, Orientation);
Point* getPreviousPoint();
#endif
