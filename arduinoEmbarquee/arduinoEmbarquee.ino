// Libraries included
#include "movements.h"
#include "Grove_I2C_Motor_Driver.h"
#include "speed.h"
#include "mapStorage.h"
#include "mapSetup.h"
#include "emitter.h"
#include "parking.h"

// Next point to be reached by ACAR
Point* destinationPoint;
// Direction which ACAR is facing
Orientation* currentOrientation;

//bool testLed = false;

// The current state of ACAR (see StatesEnum)
States state;
// The time ACAR entered the current state
long timeStateEntered;
// The time ACAR began to move forward
long speedTimer;
// Used when the car turns; true if the new path has been detected
bool seeingNewPath = false;
// ACAR's forward speed
int carSpeed = REDUCEDSPEED;

void setup() {
  // Initialization of the different communications
  Serial.begin(9600);
  Serial.println("Slt");
  Motor.begin(I2CADDR);
  // Initialization of the different pins (set on INPUT)
  pinMode(SIGNALPINEL, INPUT);
  pinMode(SIGNALPINL, INPUT);
  pinMode(SIGNALPINR, INPUT);
  pinMode(SIGNALPINER, INPUT);

  pinMode(PIN_LEFT_LED, OUTPUT);
  pinMode(PIN_CENTER_LED, OUTPUT);
  pinMode(PIN_RIGHT_LED, OUTPUT);



  // Motors are stopped by default
  // Speed and direction of the motors range between -100 (backward) and 100 (forward)
  Motor.stop(MOTOR1); // Left motors set
  Motor.stop(MOTOR2); // Right motors set

  // randomSeed() on an unused pin, to make sure different seed numbers are generated each time the program runs. randomSeed() is used to shuffle the random function.
  randomSeed(analogRead(0));
  // The delay before the engine starts
  delay(START);
  Serial.println("Vroum");
  // Acceleration procedure
  Motor.speed(MOTOR1, STARTINGSPEED);
  Motor.speed(MOTOR2, STARTINGSPEED);
  delay(200);
  Motor.speed(MOTOR1, FULLSPEED);
  Motor.speed(MOTOR2, FULLSPEED);
  // Initialization of the first orientation of the car and its destination
  currentOrientation = (Orientation*)malloc(sizeof(Orientation));
  destinationPoint = mapSetup();
  *currentOrientation = NEG_X;
  // The first junction used to go from 0 to 1
  // &destinationPoint will change from the actual destination to the next one, state will change from the current way of moving to the one used to move on a junction
  initJunction(&destinationPoint, currentOrientation, &state);
}

// The main loop of the arduino
void loop() {
  // The function used to calculate ACAR's speed
  speedCalc();
  // Variables initialization for the sensors ; LOW = White detected, HIGH = BLACK detected
  bool leftSensor         = LOW == digitalRead(SIGNALPINL);
  bool extremeLeftSensor  = LOW == digitalRead(SIGNALPINEL);
  bool rightSensor        = LOW == digitalRead(SIGNALPINR);
  bool extremeRightSensor = LOW == digitalRead(SIGNALPINER);


  // Changing the car's state if the minimum time between 2 junctions has passed
  if (state == FORWARD && timeStateEntered < millis() - TIMEBETWEENJUNCS &&
      !(
        !(extremeLeftSensor || extremeRightSensor)
        || (extremeLeftSensor && !leftSensor && rightSensor)
        || (!rightSensor && extremeLeftSensor && extremeRightSensor)
      )
     )
  {
    Serial.println("Entering junction");
    // Changes the destination to the next point which ACAR can access by moving forward, also changes the current orientation in order to move to this point, and the state ACAR needs to be
    initJunction(&destinationPoint, currentOrientation, &state);
    // Updates the timer that signals a state changement
    timeStateEntered = millis();
  }
  // Same thing as before except the timer is different
  else if (state == LEFT_JUNCTION && timeStateEntered < millis() - TURNMINDURATION) {
    if (leftSensor) {
      // Indicates that ACAR knows what it has to do after the junction
      seeingNewPath = true;
    }
    // Now that ACAR has a new path, it can end maneuvering in the junction
    else if (seeingNewPath) {
      Serial.println("Ending left junc");
      endLeftJunction(&state, &timeStateEntered);
      seeingNewPath = false;
    }
  }
  else if (state == RIGHT_JUNCTION && timeStateEntered < millis() - TURNMINDURATION) {
    if (rightSensor) {
      seeingNewPath = true;
    }
    else if (seeingNewPath) {
      Serial.println("Ending right junc");
      endRightJunction(&state, &timeStateEntered);
      seeingNewPath = false;
    }
  }
  // Indicates ACAR it has to move forward at the junction
  else if (state == FORWARD_JUNCTION && timeStateEntered < millis() - STRAIGHTJUNCTDURATION) {
    Serial.println("Ending straight junc");
    endStraightJunction(&state, &timeStateEntered);
    speedTimer = millis();
  }
  // The trajectory correction done after turning
  else if (state == AFTER_TURN_CORRECTION && timeStateEntered < millis() - CORRAFTERTURNDURATION) {
    Serial.println("Ended turn corr");
    state = FORWARD;
    Motor.stop(MOTOR1);
    Motor.stop(MOTOR2);
    timeStateEntered = millis();
    speedTimer = millis();
  }
  //testLed = !testLed;
  //  digitalWrite(PIN_LEFT_LED, (testLed ? LOW : HIGH));

  // The switch used to turn on and off the different lights that were used for debug
  switch (state)
  {
    case FORWARD:
      digitalWrite(PIN_LEFT_LED  , LOW);
      digitalWrite(PIN_CENTER_LED, LOW);
      digitalWrite(PIN_RIGHT_LED , LOW);
      break;
    case LEFT_JUNCTION:
      digitalWrite(PIN_LEFT_LED  , HIGH);
      digitalWrite(PIN_CENTER_LED, HIGH);
      digitalWrite(PIN_RIGHT_LED , LOW);
      break;
    case RIGHT_JUNCTION:
      digitalWrite(PIN_LEFT_LED  , LOW);
      digitalWrite(PIN_CENTER_LED, HIGH);
      digitalWrite(PIN_RIGHT_LED , HIGH);
      break;
    case FORWARD_JUNCTION:
      digitalWrite(PIN_LEFT_LED  , LOW);
      digitalWrite(PIN_CENTER_LED, HIGH);
      digitalWrite(PIN_RIGHT_LED , LOW);
      break;
    case AFTER_TURN_CORRECTION:
      digitalWrite(PIN_CENTER_LED, LOW);
      break;
  }

  // Taking actions

  // ACAR moves forward in an acceleration pattern
  if (state == FORWARD || state == FORWARD_JUNCTION) {
    Serial.print("^");
    if (speedTimer < millis() - ACCELERATIONTIME) {
      carSpeed = FULLSPEED;
    }
    else {
      carSpeed = REDUCEDSPEED;
    }
    // The right correction used to correct ACAR's trajectory while moving forward
    if (!extremeLeftSensor && !leftSensor && rightSensor && !extremeRightSensor) {
      correctRight(carSpeed);
    }
    // The left correction used to correct ACAR's trajectory while moving forward
    else if (!extremeLeftSensor && leftSensor && !rightSensor && !extremeRightSensor) {
      correctLeft(carSpeed);
    }
    // The left correction used to correct ACAR's trajectory after turning left
    else if (extremeLeftSensor && !leftSensor && !rightSensor && !extremeRightSensor) {
      bigCorrectLeft(carSpeed);
    }
    // The right correction used to correct ACAR's trajectory after turning right
    else if (!extremeLeftSensor && !leftSensor && !rightSensor && extremeRightSensor) {
      bigCorrectRight(carSpeed);
    }
    // ACAR goes straight
    else goStraight(carSpeed);
  }
  // Changing the car's state if the minimum time before a turn has passed, used to let the car roll a little bit more before turning, to ensure it has the best position to turn
  if (state == LEFT_JUNCTION && timeStateEntered < millis() - (carSpeed == FULLSPEED ? DELAYBEFORETURN : DELAYBEFORETURNSLOW)) {
    Serial.print("<");
    turnLeft();
  }
  else if (state == RIGHT_JUNCTION && timeStateEntered < millis() - (carSpeed == FULLSPEED ? DELAYBEFORETURN : DELAYBEFORETURNSLOW)) {
    Serial.print(">");
    turnRight();
  }

  /*

    // Moving functions depending on the sensors' events
    if (leftSensor && !extremeLeftSensor && !rightSensor && !extremeRightSensor) {
    correctLeft();
    }
    else if (rightSensor && !extremeLeftSensor && !leftSensor && !extremeRightSensor) {
    correctRight();
    }
    else if (!(!(extremeRightSensor && extremeLeftSensor) || extremeLeftSensor &&  !leftSensor && rightSensor
    || extremeRightSensor && !rightSensor && extremeLeftSensor)
    || !leftSensor && !extremeLeftSensor && rightSensor && extremeRightSensor || extremeRightSensor
    || leftSensor && extremeLeftSensor && !rightSensor && !extremeRightSensor || extremeLeftSensor) {

    //delay(JUNCTION);
    junction(&currentPoint, currentOrientation);
    }

    else if (leftSensor && extremeLeftSensor && !rightSensor && !extremeRightSensor || extremeLeftSensor) {
    delay(JUNCTION);
    digitalWrite(PIN_LEFT_LED,HIGH);
    do{
     turnLeft();
     leftSensor = LOW == digitalRead(SIGNALPINL);
    }while(!leftSensor);
    digitalWrite(PIN_LEFT_LED,LOW);
    }


    else if (!leftSensor && !extremeLeftSensor && rightSensor && extremeRightSensor || extremeRightSensor) {
    delay(JUNCTION);
    digitalWrite(PIN_RIGHT_LED,HIGH);
    do{
     turnRight();
     rightSensor = LOW == digitalRead(SIGNALPINR);
    }while(!rightSensor);
    digitalWrite(PIN_RIGHT_LED,LOW);
    }

    else if (!rightSensor && !extremeLeftSensor && !leftSensor && !extremeRightSensor) {
    delay(JUNCTION);
    goStraight();
    }

    if (solveParkingSpot() > 0){
    Serial.println(solveParkingSpot());
    }
  */
  //Serial.println(micros());

  // Everytime a parking spot is found, it gets printed here
  float parkingSpot = solveParkingSpot();

  if (parkingSpot > 0) {
    Serial.print("PakingSpace : ");
    Serial.println(parkingSpot);
  }

  // The function that emitts a message
  broadcast(destinationPoint->id, getPreviousPoint()->id, parkingSpot);

}
