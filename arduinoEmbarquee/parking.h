#include "speed.h"

#define IR_PROXIMITY_SENSOR A2// Analog input pin that  is attached to the sensor
#define ADC_REF 5 //reference voltage of ADC is 5v.If the Vcc switch on the Seeeduino
                  //board switches to 3V3, the ADC_REF should be 3.3

// The minimum size of a parking spot
#define MINSLOTSIZE 27

// The function used to find the parking spots
float solveParkingSpot();

// The function used to get the value of the voltage
float getVoltage();

