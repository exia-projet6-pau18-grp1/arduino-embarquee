#include <VirtualWire.h>
#include "mapStorage.h"
#include "emitter.h"

//Grove - 315(433) RF link kit Demo v1.0
//by :http://www.seeedstudio.com/
//connect the sent module to D2 to use



// Emitts a message composed of the previous point ACAR was on, the current point ACAR is standing on, and the length of the parking spot marked (centimeters)
void broadcast(int currentPoint, int previousPoint, float parkingSpot)
{

  vw_set_tx_pin(RF_TX_PIN); // Setup transmit pin
  vw_setup(12000); // Transmission speed in bits per second.

  // The message, containing the parameters of broadcast
  Frame message;

  // Reads the analog pins and prints the message
  message.currentPoint = currentPoint ;
  message.previousPoint = previousPoint;
  message.sizeOfSpot = parkingSpot;
  /*
  Serial.println("envoie du message : ");
  Serial.println(message.previousPoint);
  Serial.println(message.currentPoint);
  Serial.println(message.sizeOfSpot);
  Serial.println(vw_send((byte *) &message, sizeof(message)));
  */

  // Sends the message
  vw_send((byte *) &message, sizeof(message));
}
