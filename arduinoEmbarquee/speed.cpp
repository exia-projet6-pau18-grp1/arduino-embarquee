// Libraries included
#include <Arduino.h>
#include "speed.h"


unsigned long prevmillis; // Used to store time
unsigned long duration; // Used to store time difference
unsigned long refresh; // Used to store the time before a new reading is done (refresh of reading)

int rpm; // RPM value
float speed; // m/s

float d = (PI * DIAMETER) / 20; // Distance between two holes
float distance; // Distance traveled by ACAR

bool currentstate; // Current state of IR input scan
bool prevstate; // State of IR sensor in previous scan

// The function used to calculate ACAR's speed
void speedCalc()
{
  // RPM Measurement
  currentstate = digitalRead(dataIN); // Read IR sensor state
  if ( prevstate != currentstate) // If there is change in input
  {
    if ( currentstate == true ) // If input only changes from LOW to HIGH
    {
      duration = ( micros() - prevmillis ); // Time difference between revolution in microsecond
      rpm = (60000000 / duration); // rpm = (1/ time millis)*1000*1000*60;
      prevmillis = micros(); // store time for nect revolution calculation
    }
    speed = d * (rpm / 60); // speed in m/s
    distance  = distance + (DIAMETER * PI) / 20;
  }
  prevstate = currentstate; // store this scan (prev scan) data for next scan

  if ( ( millis() - refresh ) >= 1000 )
  {
    /*Serial.print("vitesse : ");
      Serial.print(speed);
      Serial.print(", distance parcourue : ");
      Serial.println(distance);
    */
    refresh = millis();
  }
}
