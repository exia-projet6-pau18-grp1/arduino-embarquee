#ifndef SPEED_H
#define SPEED_H

// The IR sensor INPUT
const int dataIN = 2;

// The wheel diameter (meters)
const float DIAMETER = 0.065;

// The function used to calculate ACAR's speed
void speedCalc();

#endif
