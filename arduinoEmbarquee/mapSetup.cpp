#include "mapStorage.h"
#include "mapSetup.h"

// The points present on the map declarations
Point *point0;
Point *point1;
Point *point2;
Point *point3;
Point *point4;
Point *point5;
Point *point6;
Point *point7;
Point *point8;
Point *point9;
Point *point10;
Point *point11;
Point *point12;
Point *point13;
Point *point14;
Point *point15;
Point *point16;

// The map ACAR is going to use. Each point is represented by its id and its coordinates (x,y)
Point* mapSetup() {
  point0 = addPoint(0, 3.6, 0);
  point1 = addPoint(1, 1.7, 0);
  point2 = addPoint(2, 0, 0);
  point3 = addPoint(3, 0, 1.3);
  point4 = addPoint(4, 0, 3);
  point5 = addPoint(5, 0, 5);
  point6 = addPoint(6, 1.7, 5);
  point7 = addPoint(7, 1.7, 4);
  point8 = addPoint(8, 6.7, 4);
  point9 = addPoint(9, 6.7, 3);
  point10 = addPoint(10, 7.8, 5);
  point11 = addPoint(11, 7.8, 4);
  point12 = addPoint(12, 7.8, 0);
  point13 = addPoint(13, 6.7, 0);
  point14 = addPoint(14, 5.3, 0);
  point15 = addPoint(15, 5.3, 1.3);
  point16 = addPoint(16, 1.7, 1.3);

  // We declare the neighbors of each point. The neighbors represent the points ACAR move to from a point
  point0->neighNegX = point1;

  point1->neighPosY = point16;
  point1->neighNegX = point2;

  point2->neighPosX = point1;
  point2->neighPosY = point3;

  point3->neighNegY = point2;
  point3->neighPosX = point16;
  point3->neighPosY = point4;

  point4->neighNegY = point3;
  point4->neighPosX = point9;
  point4->neighPosY = point5;

  point5->neighNegY = point4;
  point5->neighPosX = point6;

  point6->neighNegY = point7;
  point6->neighNegX = point5;
  point6->neighPosX = point10;

  point7->neighPosY = point6;
  point7->neighPosX = point8;

  point8->neighNegY = point9;
  point8->neighNegX = point7;

  point9->neighPosY = point8;
  point9->neighNegX = point4;
  point9->neighPosX = point11;
  point9->neighNegY = point13;

  point10->neighNegY = point11;
  point10->neighNegX = point6;

  point11->neighPosY = point10;
  point11->neighNegX = point9;
  point11->neighNegY = point12;

  point12->neighNegX = point13;
  point12->neighPosY = point11;

  point13->neighNegX = point14;
  point13->neighPosX = point12;
  point13->neighPosY = point9;

  point14->neighPosY = point15;
  point14->neighPosX = point13;

  point15->neighNegY = point14;
  point15->neighNegX = point16;

  point16->neighNegY = point1;
  point16->neighNegX = point3;
  point16->neighPosX = point15;

  // Returns the first point ACAR will be standing on
  return point0;

}
