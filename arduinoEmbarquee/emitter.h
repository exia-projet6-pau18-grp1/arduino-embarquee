#ifndef EMITTER_H
#define EMITTER_H

// The pin used to send a message
#define RF_TX_PIN 3

// Defines the Frame structure of type FrameStruct so it can be used in other files
typedef struct FrameStruct Frame;

// The FrameStruct structure
struct FrameStruct {
  int previousPoint;

  int currentPoint;

  float sizeOfSpot;
};

// The broadcast function
void broadcast(int currentPoint, int previousPoint, float parkingSpot);

#endif

