// Libraries included
#include <Arduino.h>
#include "Grove_I2C_Motor_Driver.h"
#include "movements.h"
#include "mapStorage.h"

// The last point ACAR stood on
Point* previousPoint;

// Moving functions

void turnLeft() {
  digitalWrite(PIN_LEFT_LED, HIGH);
  Motor.speed(MOTOR1, BACKWARDTURNSPEED);
  Motor.speed(MOTOR2, TURNSPEED);
}

void turnRight() {
  digitalWrite(PIN_RIGHT_LED, HIGH);
  Motor.speed(MOTOR1, TURNSPEED);
  Motor.speed(MOTOR2, BACKWARDTURNSPEED);
}

void correctRight(int maxSpeed) {
  Motor.speed(MOTOR1, maxSpeed);
  Motor.speed(MOTOR2, CORRECTIONSPEED);
}

void correctLeft(int maxSpeed) {
  Motor.speed(MOTOR1, CORRECTIONSPEED);
  Motor.speed(MOTOR2, maxSpeed);
}

void bigCorrectRight(int maxSpeed) {
  Motor.speed(MOTOR1, maxSpeed);
  Motor.speed(MOTOR2, BIGCORRECTIONSPEED);
}

void bigCorrectLeft(int maxSpeed) {
  Motor.speed(MOTOR1, BIGCORRECTIONSPEED);
  Motor.speed(MOTOR2, maxSpeed);
}


void goStraight(int maxSpeed) {
  Motor.speed(MOTOR1, maxSpeed);
  Motor.speed(MOTOR2, maxSpeed);
}

// The initJunction enables ACAR to go from a point to another one
void initJunction(Point** currentPoint, Orientation* currentOrientation, States* state) {
  Serial.println("Arrivé à une intersection");
  Serial.print("Point actuel: ");
  Serial.println((*currentPoint)->id);
  digitalWrite(PIN_CENTER_LED, HIGH);
  Point* nextPoint;
  Orientation wantedOrientation;
  Motor.stop(MOTOR1);
  Motor.stop(MOTOR2);

  // Checks if the random number pulled corresponds to an available direction
  do {
    bool isSuccessful = false;
    do {
      int randomNumber = random(4);
      //Serial.print("Nb aléatoire: ");
      //Serial.println(randomNumber);
      if (randomNumber == 0 && (*currentPoint)->neighPosX) {
        wantedOrientation = POS_X;
        isSuccessful = true;
        nextPoint = (*currentPoint)->neighPosX;
        Serial.println("Go vers +X");
      }
      else if (randomNumber == 1 && (*currentPoint)->neighNegX) {
        wantedOrientation = NEG_X;
        isSuccessful = true;
        nextPoint = (*currentPoint)->neighNegX;
        Serial.println("Go vers -X");
      }
      else if (randomNumber == 2 && (*currentPoint)->neighPosY) {
        wantedOrientation = POS_Y;
        isSuccessful = true;
        nextPoint = (*currentPoint)->neighPosY;
        Serial.println("Go vers +Y");
      }
      else if (randomNumber == 3 && (*currentPoint)->neighNegY) {
        wantedOrientation = NEG_Y;
        isSuccessful = true;
        nextPoint = (*currentPoint)->neighNegY;
        Serial.println("Go vers -Y");
      }
    } while (!isSuccessful);
  } while (wantedOrientation == reverse(*currentOrientation));
  Serial.println("=== Destination choisie ===");
  Serial.print("Point suivant: ");
  Serial.println(nextPoint->id);

  // ACAR is moving, so the point it wanted to go to became the point it's standing on ; and the point it wants to go to becomes the next point
  previousPoint = *currentPoint;
  *currentPoint = nextPoint;
  Serial.print("Nouveau point actuel: ");
  Serial.println((*currentPoint)->id);

  // Checks if the current orientation and the wanted orientation picture a left turn
  if (isLeftTurn(wantedOrientation, *currentOrientation)) {
    *state = LEFT_JUNCTION;
  }
  else if (isRightTurn(wantedOrientation, *currentOrientation)) {
    *state = RIGHT_JUNCTION;
  }
  else if (isNoTurn(wantedOrientation, *currentOrientation)) {
    *state = FORWARD_JUNCTION;
  }
  // ACAR's current orientation is now the orientation it wanted to have
  *currentOrientation = wantedOrientation;
  Serial.print("Nouvelle orientation: ");
  Serial.println(*currentOrientation);
}

// Ends the junction maneuvering after a left turn
void endLeftJunction(States *state, long* timer) {
  *timer = millis();
  *state = AFTER_TURN_CORRECTION;
  bigCorrectRight(FULLSPEED);
}

// Ends the junction maneuvering after a right turn
void endRightJunction(States *state, long* timer) {
  *timer = millis();
  *state = AFTER_TURN_CORRECTION;
  bigCorrectLeft(FULLSPEED);
}
// Ends the junction maneuvering after moving forward, no corrections are needed here
void endStraightJunction(States *state, long* timer) {
  *timer = millis();
  *state = FORWARD;
}

/*void junction(Point** currentPoint, Orientation* currentOrientation) {
  Serial.println("Arrivé à une intersection");
  Serial.print("Point actuel: ");
  Serial.println((*currentPoint)->id);
  digitalWrite(PIN_CENTER_LED, HIGH);
  Point* nextPoint;
  Orientation wantedOrientation;
  Motor.stop(MOTOR1);
  Motor.stop(MOTOR2);
  do {
    bool isSuccessful = false;
    do {
      int randomNumber = random(4);
      //Serial.print("Nb aléatoire: ");
      //Serial.println(randomNumber);
      if (randomNumber == 0 && (*currentPoint)->neighPosX) {
        wantedOrientation = POS_X;
        isSuccessful = true;
        nextPoint = (*currentPoint)->neighPosX;
        Serial.println("Go vers +X");
      }
      else if (randomNumber == 1 && (*currentPoint)->neighNegX) {
        wantedOrientation = NEG_X;
        isSuccessful = true;
        nextPoint = (*currentPoint)->neighNegX;
        Serial.println("Go vers -X");
      }
      else if (randomNumber == 2 && (*currentPoint)->neighPosY) {
        wantedOrientation = POS_Y;
        isSuccessful = true;
        nextPoint = (*currentPoint)->neighPosY;
        Serial.println("Go vers +Y");
      }
      else if (randomNumber == 3 && (*currentPoint)->neighNegY) {
        wantedOrientation = NEG_Y;
        isSuccessful = true;
        nextPoint = (*currentPoint)->neighNegY;
        Serial.println("Go vers -Y");
      }
    } while (!isSuccessful);
  } while (wantedOrientation == reverse(*currentOrientation));


  Serial.println("=== Destination choisie ===");
  Serial.print("Point suivant: ");
  Serial.println(nextPoint->id);
   currentPoint = nextPoint;
  Serial.print("Nouveau point actuel: ");
  Serial.println((*currentPoint)->id);

  if (isLeftTurn(wantedOrientation, *currentOrientation)) {
    Serial.println("Go vers la gauche");
    digitalWrite(PIN_LEFT_LED, HIGH);
    delay(DELAYBEFORETURN);
    turnLeft();
    delay(TURNMINDURATION);
    do {
      turnLeft();
    } while (HIGH == digitalRead(SIGNALPINL));
    digitalWrite(PIN_RIGHT_LED, HIGH);
    correctRight();
    //delay(110);
    digitalWrite(PIN_RIGHT_LED, LOW);
    digitalWrite(PIN_LEFT_LED, LOW);

  } else if (isRightTurn(wantedOrientation, *currentOrientation)) {
    Serial.println("Go vers la droite");
    digitalWrite(PIN_RIGHT_LED, HIGH);
    delay(DELAYBEFORETURN);
    turnRight();
    delay(TURNMINDURATION);
    do {
      turnRight();
    } while (HIGH == digitalRead(SIGNALPINR));
    digitalWrite(PIN_LEFT_LED, HIGH);
    correctLeft();
    //delay(110);
    digitalWrite(PIN_LEFT_LED, LOW);
    digitalWrite(PIN_RIGHT_LED, LOW);

  } else if (mustGoStraight(wantedOrientation, *currentOrientation)) {
    Serial.println("Go tout droit");
    goStraight();
    delay(300);
  }
   currentOrientation = wantedOrientation;
  Serial.print("Nouvelle orientation: ");
  Serial.println(*currentOrientation );
  digitalWrite(PIN_CENTER_LED, LOW);
  Serial.println("FIN INTERSECTION");
  }
*/

// Checks if the current orientation and the wanted orientation are describing a right turn
bool isRightTurn(Orientation wantedOrientation, Orientation currentOrientation) {
  return currentOrientation == NEG_X && wantedOrientation == NEG_Y
         || currentOrientation == NEG_Y && wantedOrientation == POS_X
         || currentOrientation == POS_X && wantedOrientation == POS_Y
         || currentOrientation == POS_Y && wantedOrientation == NEG_X;
}

// Checks if the current orientation and the wanted orientation are describing a left turn
bool isLeftTurn(Orientation wantedOrientation, Orientation currentOrientation) {
  return currentOrientation == NEG_X && wantedOrientation == POS_Y
         || currentOrientation == NEG_Y && wantedOrientation == NEG_X
         || currentOrientation == POS_X && wantedOrientation == NEG_Y
         || currentOrientation == POS_Y && wantedOrientation == POS_X;
}

//Checks if the current orientation and the wanted orientation are not describing any turn
bool isNoTurn(Orientation wantedOrientation, Orientation currentOrientation) {
  return currentOrientation == wantedOrientation;
}

// Gets the previous point ACAR stood on
Point* getPreviousPoint() {
  return previousPoint;
}



