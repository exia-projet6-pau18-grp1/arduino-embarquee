#include "mapStorage.h"
#include <stdio.h>
#include <stdlib.h>

// Creates a new point
Point* addPoint(int id, int x, int y)
{
  Point* newPoint = malloc(sizeof(Point));

  newPoint->x = x;

  newPoint->y = y;

  newPoint->id = id;

  newPoint->neighPosX = NULL;

  newPoint->neighPosY = NULL;

  newPoint->neighNegX = NULL;

  newPoint->neighNegY = NULL;

  return newPoint;
}

// Inserts a PointElem at the end of a Point linked list
void insertAtEnd(PointElem* &first, PointElem* newElem) {

  if (!first) { // empty list becomes the new node
    first = newElem;
    return;
  } else { // find last and link the new node
    PointElem* last = first;
    while (last->next) last = last->next;
    last->next = newElem;
  }
}

// Returns the reversed Orientation
Orientation reverse(Orientation currentOrientation) {
  switch (currentOrientation) {
    case POS_X :
      return NEG_X;
      break;

    case NEG_X :
      return POS_X;
      break;

    case POS_Y :
      return NEG_Y;
      break;

    case NEG_Y :
      return POS_Y;
      break;
  }
}

