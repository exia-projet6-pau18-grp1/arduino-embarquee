#ifndef MAPSTORAGE_H
#define MAPSTORAGE_H

// Represents a point
typedef struct PointStruct Point;

// Represents a node in a Point linked list
typedef struct PointElemStruct PointElem;

// Represents a linked list of Points
typedef PointElem* listPoint;

struct PointElemStruct {
  Point* element;
  PointElem* next;
};

/*
   Represents a point
*/
struct PointStruct {
  int id;
  // x coordinate
  int x;
  // y coordinate
  int y;

  //neighbours
  Point* neighPosX;
  Point* neighPosY;
  Point* neighNegX;
  Point* neighNegY;
};

// Creates a new point
Point* addPoint(int id, int x, int y);

enum OrientationEnum {POS_X, NEG_X, POS_Y, NEG_Y};

// An orientation
typedef enum OrientationEnum Orientation;

// Returns the reversed Orientation
Orientation reverse(Orientation);


#endif // !MAPSTORAGE_H
