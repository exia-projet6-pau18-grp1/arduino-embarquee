#include "parking.h";
#include <Arduino.h>

// The variable used to create the timer used when ACAR leaves a parking spot area
float beginTime;

// The boolean used to check if ACAR has entered a parking spot area or not
bool crossingSpot = false;


float solveParkingSpot()
{
  float voltage = getVoltage();

  // ACAR has passed close to a parking spot area
  if (voltage <= 1.9 && !crossingSpot) {
    beginTime = millis();
    crossingSpot = true;
  }

  // If ACAR was close to a parking spot area and it left it, we calculate the parking spot's size
  else if (voltage > 1.9 && crossingSpot) {
    float timer = (millis() - beginTime) / 1000;
    Serial.println(timer);
    // float size = speedCalc() * 100 * timer;
    float size = 60 * timer;
    crossingSpot = false;

    // If the size of the parking spot calculated earlier is superior to the minimum size of a parking spot, we conclude it is a parking spot and we print its size
    if (size > MINSLOTSIZE) {
      Serial.print("taille : ");
      Serial.println(size);
      return size;
    }
  }
  return 0;
}

/****************************************************************************/
/*Function: Get voltage from the sensor pin that is connected with analog pin*/
/*Parameter:-void                                                       */
/*Return:   -float,the voltage of the analog pin
*/

// The function that gets the voltage of the sensor
float getVoltage()
{
  float sensor_value;
  // int sum;

  // Reads the analog pin to get the sensor's value
  sensor_value = analogRead(IR_PROXIMITY_SENSOR);
  // read the analog in value:
  /*for (int i = 0;i < 20;i ++)//Continuous sampling 20 times
    {

      sum += sensor_value;
    }
    sensor_value = sum / 20;

  */

  // The variable voltage calculates the voltage based on the value picked up earlier
  float voltage;
  voltage = sensor_value * ADC_REF / 1024;
  Serial.println(voltage);
  return voltage;
}

